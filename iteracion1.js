/* Dado el siguiente javascript usa for of para recorrer el array de películas

, genera un nuevo array con las categorías de las películas

e imprime por consola el array de categorías.Ten en cuenta que las categorías no deberían repetirse.Para filtrar las categorías puedes ayudarte de la función .includes() */

const movies = [
    {title: 'Madaraspar', duration: 192, categories: ['comedia', 'aventura']},
    {title: 'Spiderpan', duration: 122, categories: ['aventura', 'acción']},
    {title: 'Solo en Whatsapp', duration: 223, categories: ['comedia', 'thriller']},
    {title: 'El gato con guantes', duration: 111, categories: ['comedia', 'aventura', 'animación']},
]

let movieGroup = [];

for( let movie of movies){  //Para OBJETOS del ARRAY
    const types = movie.categories;// Creamos VARIABLE con valor de la localizacion del ARRAY
    for( let a of types){//Para ELEMENTOS del ARRAY del OBJETO
        if(!movieGroup.includes(a))//Si el NUEVO-ARRAY no incluye ELEMENTO
        movieGroup.push(a);//RECOGE EL ELEMENTO
    }
}
console.log(movieGroup)